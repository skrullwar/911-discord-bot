from dataclasses import dataclass
from config.config_models.discord_config import DiscordConfig
from config.config_models.user_config import UserConfig
from config.config_models.database_config import DatabaseConfig
import configparser


@dataclass
class ConfigWrapper(configparser.ConfigParser):
    """a simple class to manage your config as objects"""

    discord_config = DiscordConfig
    user_config: UserConfig
    database_config: DatabaseConfig

    def __init__(self, path: str):
        super().__init__()
        self.path = path

    def read(self, filename=None, encoding=None):
        super().read(self.path if not filename else filename)
        self.discord_config = DiscordConfig(**self._sections[DiscordConfig.__name__])
        self.user_config = UserConfig(**self._sections[UserConfig.__name__])
        self.database_config = DatabaseConfig(**self._sections[DatabaseConfig.__name__])


if __name__ == '__main__':
    conf = ConfigWrapper('config.ini')
    conf.read()
    print(conf.discord_config.__dict__)
    print(conf.user_config.__dict__)
    print(conf.database_config.__dict__)
