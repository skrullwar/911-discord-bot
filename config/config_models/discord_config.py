from dataclasses import dataclass


@dataclass
class DiscordConfig:

    def __init__(self, discord_token=None):
        self.discord_token = discord_token
