from dataclasses import dataclass


@dataclass
class DatabaseConfig:

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
