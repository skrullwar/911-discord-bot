from dataclasses import dataclass


@dataclass
class UserConfig:

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.__get_lists()

    def __get_lists(self):
        for key, value in self.__dict__.items():
            if type(value) == str and '", "' in value:
                setattr(self, key, eval(value))
