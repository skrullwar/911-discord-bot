import asyncio
from datetime import datetime
import discord
import time



client = discord.Client()


@client.event
async def on_ready():
    print('Logged.\n')


@client.event
async def on_message(message):
    now2 = datetime.now()
    current_time = now2.strftime("%H:%M:%S")
    print("Current Time =", current_time)

    if message.content.startswith('$thumb'):
        channel = message.channel
        await channel.send('Send me that 👍 reaction, mate')

        def check(reaction, user):
            return user == message.author and str(reaction.emoji) == '👍'

        try:
            reaction, user = await client.wait_for('reaction_add', timeout=60.0, check=check)
        except asyncio.TimeoutError:
            await channel.send('👎')
        else:
            await channel.send('👍')

    elif message.content.startswith('$tortura') and message.author.id == 123456789:  # TODO: add to config
        print(message.author.id)
        channel = message.channel
        await channel.send('Torture Process Started.. :wrench: :hammer: ...')
    elif message.content.startswith('call the police'):  # TODO: add multilanguage
        print(message.author.id)
        channel = message.channel
        await call_police(channel)


def check():
    now = datetime.now()
    reminder_time1 = now.replace(hour=19, minute=45, second=0, microsecond=0)
    reminder_time2 = now.replace(hour=19, minute=45, second=59, microsecond=0)
    print(f"{reminder_time1} < {now} < {reminder_time2} = {reminder_time1 < now < reminder_time2}")
    return reminder_time1 < now < reminder_time2


async def remind(channel):
    try:
        await channel.send(':new_moon_with_face:')
        time.sleep(60)
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        await channel.send('@everyone OH OH IT\'S THE TORTURE TIME!!') \
            if check() else await channel.send(f'IT\'S  : {current_time}')
        await remind(channel)
    except asyncio.TimeoutError:
        pass
    else:
        pass
    await remind(channel)


async def call_police(channel):
    try:
        await channel.send(':man_police_officer:')
        await channel.send('@pericope97 you\'re under arrest. You have the right to remain silent. Anything you '
                           'say can and will be used against you in a court of law. You have the right to have an '
                           'attorney present during questioning. If you cannot afford an attorney, one will be '
                           'appointed for you.')
    except asyncio.TimeoutError:
        pass
    else:
        pass


if __name__ == "__main__":
    client.run(TOKEN)

