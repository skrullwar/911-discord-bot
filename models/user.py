from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from config.config_wrapper import ConfigWrapper


Base = declarative_base()
config = ConfigWrapper('../config/config.ini')
config.read()
Engine = create_engine(config.database_config.path, echo=True)


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    discord_id = Column(Integer)
    first_name = Column(String)
    last_name = Column(String)
    username = Column(String)
    created_at = Column(Integer)

    warnings = relationship("warning", back_populates="user")

    def __repr__(self):
        return f"<User(ID={self.id}, discord_id={self.discord_id}, first_name={self.first_name}," \
               f" last_name={self.last_name}," \
               f" username={self.username})>"


if __name__ == "__main__":
    print("test")
    Base = declarative_base()
    config = ConfigWrapper('../config/config.ini')
    config.read()

    print(config.database_config.path)
