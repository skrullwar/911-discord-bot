from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from config.config_wrapper import ConfigWrapper

Base = declarative_base()
config = ConfigWrapper('../config/config.ini')
config.read()
Engine = create_engine(config.database_config.path, echo=True)


class Warnings(Base):
    __tablename__ = 'warning'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    created_at = Column(Integer, primary_key=True)

    def __repr__(self):
        return f"<Warning(group={self.group_id}, victim={self.user_id}, time={self.created_at})>"

    user = relationship("User", back_populates="warnings")


if __name__ == "__main__":
    print("test")
    Base = declarative_base()
    config = ConfigWrapper('../config/config.ini')
    config.read()

    print(config.database_config.path)
